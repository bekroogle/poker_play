/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package poker;

/**
 *
 * @author bekro_000
 */
public class Card {
  Card(char f_face_value, int f_suit) {
    face_value = f_face_value;
    suit = f_suit;
    in_play = false;
  }
  
  public void deal_card() {
    in_play = true;
  }
  public String toString() {
    return (face_value + " of " + Deck.get_suit_name(suit));
  }
  char face_value;
  int suit;
  Boolean in_play;
}
