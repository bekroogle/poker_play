/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package poker;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author bekro_000
 */
public final class Deck {
  
  /**
   * get_suit_name
   * @param suit_num the integer representation of the card's suit (i.e. 0)
   * @return the string representation of the card's suit (i.e. "Hearts")
   */
  public static String get_suit_name(int suit_num) {
    switch(suit_num) {
      case 0: return "Hearts";
      case 1: return "Diamonds";
      case 2: return "Clubs";
      case 3: return "Spades";
      default: return null;
    }
  }
  
  
  Deck() {
    init_values();
    init_cards();
  }
  ArrayList<Card> cards;
  char[] values;

  /**
   *
   */
  public void init_values() {
    values = new char[13];
    values[0] = '2';
    values[1] = '3';
    values[2] = '4';
    values[3] = '5';
    values[4] = '6';
    values[5] = '7';
    values[6] = '8';
    values[7] = '9';
    values[8] = 'T';
    values[9] = 'J';
    values[10] = 'Q';
    values[11] = 'K';
    values[12] = 'A'; 
  }
  
  public void init_cards() {
    cards = new ArrayList<>();
    for (int i = 0; i < 52; i++) {
      cards.add(new Card(values[i % 13], i / 13));
    }
  }
  
  public void shuffle() {
    Collections.shuffle(cards);
    for (Card c : cards) {
      c.in_play = false;
    }
  }
  
  public Card deal_cards() /*throws EmptyDeckExcept*/{
    for (Card c : cards) {
      if (!c.in_play) {
        c.deal_card();
        return c;
      }
    }
    return null;
//    throw new EmptyDeckExcept();
  }
  
  public Card[] deal_cards(int howMany){
    Card[] returnDeck;
    returnDeck= new Card[howMany];
    
    for (int i = 0; i < howMany; i++) {
//      try {
        returnDeck[i] = deal_cards();
//      } catch (EmptyDeckExcept e) {
//        System.err.println(e.toString());
//      }
      
    }
    return returnDeck;
    
  }
}


